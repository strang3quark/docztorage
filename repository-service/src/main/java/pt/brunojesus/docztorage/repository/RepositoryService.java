package pt.brunojesus.docztorage.repository;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RepositoryService {

    public static void main(String[] args) {
        SpringApplication.run(RepositoryService.class, args);
    }
}
