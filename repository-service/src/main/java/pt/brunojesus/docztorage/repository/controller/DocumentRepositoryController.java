package pt.brunojesus.docztorage.repository.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pt.brunojesus.docztorage.repository.metadata.model.dto.DocumentMetadata;
import pt.brunojesus.docztorage.repository.service.DocumentTransferService;

import java.io.IOException;

@Log4j2
@RestController
public class DocumentRepositoryController {

    private final DocumentTransferService documentTransferService;

    @Autowired
    public DocumentRepositoryController(DocumentTransferService documentTransferService) {
        this.documentTransferService = documentTransferService;
    }

    @PostMapping("/document")
    public DocumentMetadata uploadDocument(@RequestParam("file") MultipartFile file) throws IOException {
        return documentTransferService.createDocument(file.getName(), file.getBytes());
    }

    @PostMapping("/document/{documentId}/version")
    public DocumentMetadata uploadFileVersion(
            @PathVariable("documentId") String documentId,
            @RequestParam("file") MultipartFile file) throws IOException {

        return documentTransferService.addDocumentVersion(documentId, file.getBytes());
    }

    @GetMapping(value = "/document/{documentId}")
    public DocumentMetadata getDocumentMetadata(@PathVariable("documentId") String documentId) {
        return documentTransferService.getDocumentMetadataById(documentId);
    }

    @GetMapping(value = "/document/{documentId}/version/{fileVersionId}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public byte[] getFile(
            @PathVariable("documentId") String documentId,
            @PathVariable("fileVersionId") String fileVersionId) {

        return documentTransferService.getDocumentFile(documentId, fileVersionId);
    }
}
