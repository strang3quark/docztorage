package pt.brunojesus.docztorage.repository.service;

import pt.brunojesus.docztorage.repository.metadata.model.dto.DocumentMetadata;

public interface DocumentTransferService {

    DocumentMetadata createDocument(String name, byte[] content);

    DocumentMetadata addDocumentVersion(String documentId, byte[] content);

    DocumentMetadata getDocumentMetadataById(String documentId);

    byte[] getDocumentFile(String documentId, String fileVersionId);
}
