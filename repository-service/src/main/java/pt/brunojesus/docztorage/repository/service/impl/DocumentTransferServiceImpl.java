package pt.brunojesus.docztorage.repository.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.brunojesus.docztorage.repository.encryption.facade.EncryptionFacade;
import pt.brunojesus.docztorage.repository.files.facade.FileRepositoryFacade;
import pt.brunojesus.docztorage.repository.metadata.facade.RepositoryMetadataFacade;
import pt.brunojesus.docztorage.repository.metadata.model.dto.DocumentMetadata;
import pt.brunojesus.docztorage.repository.service.DocumentTransferService;

import java.util.UUID;

@Service
public class DocumentTransferServiceImpl implements DocumentTransferService {

    private final RepositoryMetadataFacade repositoryMetadataFacade;
    private final FileRepositoryFacade fileRepositoryFacade;
    private final EncryptionFacade encryptionFacade;

    @Autowired
    public DocumentTransferServiceImpl(RepositoryMetadataFacade repositoryMetadataFacade,
                                       FileRepositoryFacade fileRepositoryFacade,
                                       EncryptionFacade encryptionFacade) {

        this.repositoryMetadataFacade = repositoryMetadataFacade;
        this.fileRepositoryFacade = fileRepositoryFacade;
        this.encryptionFacade = encryptionFacade;
    }

    @Override
    public DocumentMetadata createDocument(String name, byte[] content) {
        UUID uuid = UUID.randomUUID();

        byte[] encryptedData = encryptionFacade.encrypt(content, uuid.toString());

        // Upload file to bucket
        fileRepositoryFacade.upload(uuid.toString(), encryptedData);

        // Create metadata
        return repositoryMetadataFacade.createDocumentMetadata(name, uuid.toString());
    }

    @Override
    public DocumentMetadata addDocumentVersion(String documentId, byte[] content) {
        UUID uuid = UUID.randomUUID();

        byte[] encryptedData = encryptionFacade.encrypt(content, uuid.toString());

        // Upload file to bucket
        fileRepositoryFacade.upload(uuid.toString(), encryptedData);

        // Create metadata
        return repositoryMetadataFacade.createDocumentMetadataVersion(documentId, uuid.toString());
    }

    @Override
    public DocumentMetadata getDocumentMetadataById(String documentId) {
        return repositoryMetadataFacade.getDocumentMetadataById(documentId);
    }

    @Override
    public byte[] getDocumentFile(String documentId, String fileVersionId) {

        DocumentMetadata documentMetadata = repositoryMetadataFacade.getDocumentMetadataById(documentId);

        if (documentMetadata == null) {
            throw new RuntimeException("Invalid documentId");
        }

        boolean exists = documentMetadata.getVersions().stream().anyMatch(v -> v.getFileId().equals(fileVersionId));

        if (!exists) {
            throw new RuntimeException("Invalid fileId");
        }

        // Get file from repo
        byte[] encryptedData = fileRepositoryFacade.download(fileVersionId);

        // Decrypt and return
        return encryptionFacade.decrypt(encryptedData, fileVersionId);
    }
}
