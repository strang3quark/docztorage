package pt.brunojesus.docztorage.repository.metadata.model.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class DocumentMetadata {

    private String documentId;
    private String fileName;
    private DocumentMetadataVersion currentVersion;
    private List<DocumentMetadataVersion> versions;
}
