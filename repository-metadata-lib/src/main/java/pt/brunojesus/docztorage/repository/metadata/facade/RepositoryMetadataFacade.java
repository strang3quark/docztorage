package pt.brunojesus.docztorage.repository.metadata.facade;

import pt.brunojesus.docztorage.repository.metadata.model.dto.DocumentMetadata;

public interface RepositoryMetadataFacade {

    DocumentMetadata getDocumentMetadataById(String documentId);

    DocumentMetadata createDocumentMetadata(String fileName, String fileId);

    DocumentMetadata createDocumentMetadataVersion(String documentId, String fileId);
}
