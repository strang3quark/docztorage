package pt.brunojesus.docztorage.repository.metadata.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pt.brunojesus.docztorage.repository.metadata.converter.DocumentMetadataEntityToDocumentMetadataConverter;
import pt.brunojesus.docztorage.repository.metadata.facade.RepositoryMetadataFacade;
import pt.brunojesus.docztorage.repository.metadata.model.dto.DocumentMetadata;
import pt.brunojesus.docztorage.repository.metadata.model.entity.DocumentMetadataEntity;
import pt.brunojesus.docztorage.repository.metadata.service.DocumentMetadataService;

@Component
public class RepositoryMetadataFacadeImpl implements RepositoryMetadataFacade {

    private final DocumentMetadataService documentMetadataService;
    private final DocumentMetadataEntityToDocumentMetadataConverter documentMetadataEntityToDocumentMetadataConverter;

    @Autowired
    public RepositoryMetadataFacadeImpl(
            DocumentMetadataService documentMetadataService,
            DocumentMetadataEntityToDocumentMetadataConverter documentMetadataEntityToDocumentMetadataConverter) {

        this.documentMetadataService = documentMetadataService;
        this.documentMetadataEntityToDocumentMetadataConverter = documentMetadataEntityToDocumentMetadataConverter;
    }

    @Override
    public DocumentMetadata getDocumentMetadataById(String documentId) {
        DocumentMetadataEntity documentMetadataEntity = documentMetadataService.getDocumentById(documentId);

        return documentMetadataEntityToDocumentMetadataConverter.convert(documentMetadataEntity);
    }

    @Override
    public DocumentMetadata createDocumentMetadata(String fileName, String fileId) {
        DocumentMetadataEntity documentMetadataEntity = documentMetadataService.createDocument(fileName, fileId);

        return documentMetadataEntityToDocumentMetadataConverter.convert(documentMetadataEntity);
    }

    @Override
    public DocumentMetadata createDocumentMetadataVersion(String documentId, String fileId) {
        DocumentMetadataEntity documentMetadataEntity = documentMetadataService.createDocumentVersion(documentId, fileId);

        return documentMetadataEntityToDocumentMetadataConverter.convert(documentMetadataEntity);
    }
}
