package pt.brunojesus.docztorage.repository.metadata.service;

import pt.brunojesus.docztorage.repository.metadata.model.entity.DocumentMetadataEntity;

public interface DocumentMetadataService {

    DocumentMetadataEntity getDocumentById(String documentId);

    DocumentMetadataEntity createDocument(String fileName, String fileId);

    DocumentMetadataEntity createDocumentVersion(String documentId, String fileId);
}
