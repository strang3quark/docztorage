package pt.brunojesus.docztorage.repository.metadata.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pt.brunojesus.docztorage.repository.metadata.model.entity.DocumentMetadataEntity;

@Repository
public interface DocumentMetadataRepository extends MongoRepository<DocumentMetadataEntity, ObjectId> {
}
