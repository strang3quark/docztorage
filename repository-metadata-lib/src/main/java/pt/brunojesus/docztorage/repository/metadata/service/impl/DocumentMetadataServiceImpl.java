package pt.brunojesus.docztorage.repository.metadata.service.impl;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.brunojesus.docztorage.repository.metadata.model.entity.DocumentMetadataEntity;
import pt.brunojesus.docztorage.repository.metadata.model.entity.DocumentMetadataVersionEntity;
import pt.brunojesus.docztorage.repository.metadata.repository.DocumentMetadataRepository;
import pt.brunojesus.docztorage.repository.metadata.service.DocumentMetadataService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class DocumentMetadataServiceImpl implements DocumentMetadataService {

    private final DocumentMetadataRepository documentMetadataRepository;

    @Autowired
    public DocumentMetadataServiceImpl(DocumentMetadataRepository documentMetadataRepository) {
        this.documentMetadataRepository = documentMetadataRepository;
    }

    @Override
    public DocumentMetadataEntity getDocumentById(String documentId) {
        return documentMetadataRepository.findById(new ObjectId(documentId))
                .orElseThrow(() -> new RuntimeException("Document not found"));
    }

    @Override
    public DocumentMetadataEntity createDocument(String fileName, String fileId) {
        List<DocumentMetadataVersionEntity> versionList = new ArrayList<>(1);

        versionList.add(DocumentMetadataVersionEntity.builder()
                .fileId(fileId)
                .dateAdded(new Date())
                .userId("TODO")
                .build()
        );

        DocumentMetadataEntity documentMetadataEntity = DocumentMetadataEntity.builder()
                .name(fileName)
                .versions(versionList)
                .build();

        return documentMetadataRepository.save(documentMetadataEntity);
    }

    @Override
    public DocumentMetadataEntity createDocumentVersion(String documentId, String fileId) {
        DocumentMetadataEntity currentDocument = getDocumentById(documentId);

        currentDocument.getVersions().stream()
                .filter(v -> v.getFileId().equals(fileId))
                .findFirst().ifPresent((a) -> {
            throw new RuntimeException("Version already exists");
        });

        currentDocument.getVersions()
                .add(DocumentMetadataVersionEntity.builder()
                        .fileId(fileId)
                        .dateAdded(new Date())
                        .userId("TODO")
                        .build());

        return documentMetadataRepository.save(currentDocument);
    }
}
