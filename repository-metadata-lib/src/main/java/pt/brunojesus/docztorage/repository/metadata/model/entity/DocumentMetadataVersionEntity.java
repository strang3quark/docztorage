package pt.brunojesus.docztorage.repository.metadata.model.entity;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class DocumentMetadataVersionEntity {

    @Id
    @EqualsAndHashCode.Include
    private ObjectId id;

    @Indexed(unique = true)
    @NotNull
    private String fileId;

    @NotNull
    private Date dateAdded;

    private String userId;
}
