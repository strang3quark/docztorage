package pt.brunojesus.docztorage.repository.metadata.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import pt.brunojesus.docztorage.repository.metadata.model.dto.DocumentMetadata;
import pt.brunojesus.docztorage.repository.metadata.model.dto.DocumentMetadataVersion;
import pt.brunojesus.docztorage.repository.metadata.model.entity.DocumentMetadataEntity;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class DocumentMetadataEntityToDocumentMetadataConverter implements Converter<DocumentMetadataEntity, DocumentMetadata> {

    @Override
    public DocumentMetadata convert(DocumentMetadataEntity documentMetadataEntity) {
        List<DocumentMetadataVersion> versions = documentMetadataEntity.getVersions().stream()
                .map(v -> DocumentMetadataVersion.builder()
                        .documentId(documentMetadataEntity.getId().toHexString())
                        .dateAdded(v.getDateAdded())
                        .fileId(v.getFileId())
                        .build()
                ).collect(Collectors.toList());


        DocumentMetadataVersion latestVersion = CollectionUtils.isEmpty(versions)
                ? null : versions.get(versions.size() - 1);

        return DocumentMetadata.builder()
                .documentId(documentMetadataEntity.getId().toHexString())
                .fileName(documentMetadataEntity.getName())
                .versions(versions)
                .currentVersion(latestVersion)
                .build();
    }
}
