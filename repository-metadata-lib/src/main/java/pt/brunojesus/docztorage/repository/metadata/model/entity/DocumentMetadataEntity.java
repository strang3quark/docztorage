package pt.brunojesus.docztorage.repository.metadata.model.entity;

import lombok.*;

import javax.validation.constraints.NotNull;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)

@Document(collection = "document-metadata")
public class DocumentMetadataEntity {

    @Id
    @EqualsAndHashCode.Include
    private ObjectId id;

    @NotNull
    private String name;

    @NotNull
    private List<DocumentMetadataVersionEntity> versions;
}
