package pt.brunojesus.docztorage.repository.metadata.model.dto;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@Builder
@EqualsAndHashCode
public class DocumentMetadataVersion {

    private String documentId;

    private String fileId;

    private Date dateAdded;
}
