package pt.brunojesus.docztorage.repository.encryption.facade;

public interface EncryptionFacade {

    byte[] encrypt(byte[] data, String key);

    byte[] decrypt(byte[] data, String key);
}
