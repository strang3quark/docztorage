package pt.brunojesus.docztorage.repository.encryption.service.impl;

import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import pt.brunojesus.docztorage.repository.encryption.service.KeyService;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.spec.KeySpec;

@Service
public class KeyServiceImpl implements KeyService {

    @Override
    @SneakyThrows
    public SecretKey generateKey(String password, byte[] iv) {
        KeySpec spec = new PBEKeySpec(password.toCharArray(), iv, 65536, 128); // AES-128
        SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] key = secretKeyFactory.generateSecret(spec).getEncoded();
        return new SecretKeySpec(key, "AES");
    }
}
