package pt.brunojesus.docztorage.repository.encryption.service.impl;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import pt.brunojesus.docztorage.repository.encryption.service.EncryptionService;
import pt.brunojesus.docztorage.repository.encryption.service.KeyService;

import javax.crypto.*;
import javax.crypto.spec.GCMParameterSpec;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.*;

@Service
public class EncryptionServiceImpl implements EncryptionService {

    private final KeyService keyService;

    @Value("${docztorage.encryption.pepper}")
    private String pepper;

    @Autowired
    public EncryptionServiceImpl(KeyService keyService) {
        this.keyService = keyService;
    }

    @Override
    public byte[] encrypt(byte[] data, String key) throws NoSuchPaddingException,
            NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException {

        //Prepare the nonce
        SecureRandom secureRandom = new SecureRandom();

        //Nonce should be 12 bytes
        byte[] iv = new byte[12];
        secureRandom.nextBytes(iv);

        //Prepare your key/password
        SecretKey secretKey = keyService.generateKey(definiteKey(key), iv);


        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        GCMParameterSpec parameterSpec = new GCMParameterSpec(128, iv);

        //Encryption mode on!
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, parameterSpec);

        //Encrypt the data
        byte[] encryptedData = cipher.doFinal(data);

        //Concatenate everything and return the final data
        ByteBuffer byteBuffer = ByteBuffer.allocate(4 + iv.length + encryptedData.length);
        byteBuffer.putInt(iv.length);
        byteBuffer.put(iv);
        byteBuffer.put(encryptedData);

        return byteBuffer.array();
    }

    @Override
    public byte[] decrypt(byte[] data, String key) throws NoSuchPaddingException,
            NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException {

        //Wrap the data into a byte buffer to ease the reading process
        ByteBuffer byteBuffer = ByteBuffer.wrap(data);

        int nonceSize = byteBuffer.getInt();

        //Make sure that the file was encrypted properly
        if (nonceSize < 12 || nonceSize >= 16) {
            throw new IllegalArgumentException("Nonce size is incorrect.");
        }
        byte[] iv = new byte[nonceSize];
        byteBuffer.get(iv);

        //Prepare your key/password
        SecretKey secretKey = keyService.generateKey(definiteKey(key), iv);

        //get the rest of encrypted data
        byte[] cipherBytes = new byte[byteBuffer.remaining()];
        byteBuffer.get(cipherBytes);

        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        GCMParameterSpec parameterSpec = new GCMParameterSpec(128, iv);

        //Encryption mode on!
        cipher.init(Cipher.DECRYPT_MODE, secretKey, parameterSpec);

        //Decrypt the data
        return cipher.doFinal(cipherBytes);
    }

    @SneakyThrows
    private String definiteKey(String key) {
        if (pepper == null) {
            throw new RuntimeException("No pepper configured!");
        }

        String hashedKey = DigestUtils.md5DigestAsHex(key.getBytes(StandardCharsets.UTF_8)).toUpperCase();
        return hashedKey + pepper;
    }
}
