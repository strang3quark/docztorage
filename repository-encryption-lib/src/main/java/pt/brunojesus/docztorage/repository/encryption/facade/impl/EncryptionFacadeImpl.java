package pt.brunojesus.docztorage.repository.encryption.facade.impl;

import lombok.SneakyThrows;
import org.springframework.stereotype.Component;
import pt.brunojesus.docztorage.repository.encryption.facade.EncryptionFacade;
import pt.brunojesus.docztorage.repository.encryption.service.EncryptionService;

@Component
public class EncryptionFacadeImpl implements EncryptionFacade {

    private final EncryptionService encryptionService;

    public EncryptionFacadeImpl(EncryptionService encryptionService) {
        this.encryptionService = encryptionService;
    }

    @Override
    @SneakyThrows
    public byte[] encrypt(byte[] data, String key) {
        return encryptionService.encrypt(data, key);
    }

    @Override
    @SneakyThrows
    public byte[] decrypt(byte[] data, String key) {
        return encryptionService.decrypt(data, key);
    }
}
