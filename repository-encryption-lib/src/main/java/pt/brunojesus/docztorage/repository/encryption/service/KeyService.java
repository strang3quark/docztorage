package pt.brunojesus.docztorage.repository.encryption.service;

import javax.crypto.SecretKey;

public interface KeyService {

    SecretKey generateKey(String password, byte[] iv);
}
