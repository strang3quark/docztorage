package pt.brunojesus.docztorage.repository.files.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.brunojesus.docztorage.repository.files.facade.FileRepositoryFacade;
import pt.brunojesus.docztorage.repository.files.service.FileStorageService;

@Service
public class FileRepositoryFacadeImpl implements FileRepositoryFacade {

    public final FileStorageService fileStorageService;

    @Autowired
    public FileRepositoryFacadeImpl(FileStorageService fileStorageService) {
        this.fileStorageService = fileStorageService;
    }

    @Override
    public void upload(String fileId, byte[] data) {
        this.fileStorageService.upload(fileId, data);
    }

    @Override
    public byte[] download(String fileId) {
        return this.fileStorageService.download(fileId);
    }
}
