package pt.brunojesus.docztorage.repository.files.service.impl;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.util.IOUtils;
import com.amazonaws.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pt.brunojesus.docztorage.repository.files.service.FileStorageService;

import java.io.ByteArrayInputStream;
import java.io.IOException;

@Service
public class FileStorageServiceImpl implements FileStorageService {

    @Value("${docztorage.files.s3.bucket}")
    private String bucketName;

    private final AmazonS3 s3client;

    @Autowired
    public FileStorageServiceImpl(AmazonS3 s3client) {
        this.s3client = s3client;
    }

    public void upload(String fileId, byte[] data) {
        s3client.putObject(new PutObjectRequest(bucketName, fileId, new ByteArrayInputStream(data), null));
    }

    @Override
    public byte[] download(String fileId) {
        byte[] result;

        S3Object s3object = s3client.getObject(bucketName, fileId);

        try {
            result = IOUtils.toByteArray(s3object.getObjectContent());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return result;
    }
}
