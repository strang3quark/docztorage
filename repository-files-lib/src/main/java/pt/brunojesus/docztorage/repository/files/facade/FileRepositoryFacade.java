package pt.brunojesus.docztorage.repository.files.facade;

public interface FileRepositoryFacade {

    void upload(String fileId, byte[] data);

    byte[] download(String fileId);
}
