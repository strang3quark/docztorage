package pt.brunojesus.docztorage.repository.files.service;

public interface FileStorageService {

    void upload(String fileId, byte[] file);

    byte[] download(String fileId);
}
