package pt.brunojesus.docztorage.repository.files.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotBlank;

@Data
@Configuration
@ConfigurationProperties(prefix = "docztorage.files.s3")
public class DoczstorageFilesConfigProperties {

    @NotBlank
    private String uri;

    private String access;

    private String secret;

    @NotBlank
    private String bucket;
}
